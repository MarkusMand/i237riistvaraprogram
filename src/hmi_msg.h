#include <avr/pgmspace.h>
#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

#define PROG_VERSION "Version: %S built on: %S %S\n"
#define LIBC_VERSION "avr-libc version: %s avr-gcc version: %s\n"
#define STUDENT_NAME "Markus Mand"
#define GET_NR_MSG "Please enter a number >"

const char n1[]  PROGMEM = "Zero";
const char n2[] PROGMEM = "One";
const char n3[]    PROGMEM = "Two";
const char n4[]    PROGMEM = "Three";
const char n5[]      PROGMEM = "Four";
const char n6[]     PROGMEM = "Five";
const char n7[]     PROGMEM = "Six";
const char n8[]     PROGMEM = "Seven";
const char n9[]     PROGMEM = "Eight";
const char n10[]     PROGMEM = "Nine";
const char n11[]     PROGMEM = "Please enter a number between 0 - 9";
PGM_P const numbers[] PROGMEM = {n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11};

#endif /* _HMI_MSG_H_ */
