#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include "hmi_msg.h"
#include "uart.h"
#include <avr/pgmspace.h>
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#define BLINK_DELAY_MS 100


static inline void init_leds(void)
{
    /* Set port A pins 22, 24 and 26 for output for Arduino Mega LED and made sure the yellow LED is constantly off */
    DDRA |= _BV(DDA0);
    DDRA |= _BV(DDA2);
    DDRA |= _BV(DDA4);
    DDRB |= _BV(DDB7);
    PORTB &= ~_BV(PORTB7);
}

static inline void init_errcon(void)
{
    uart1_init();
    stderr = &uart1_out;
    fprintf_P(stderr, PSTR(PROG_VERSION),
              PSTR(FW_VERSION), PSTR(__DATE__), PSTR(__TIME__));
    fprintf_P(stderr, PSTR(LIBC_VERSION), PSTR(__AVR_LIBC_VERSION_STRING__),
              PSTR(__VERSION__));
}
static inline void blink_leds(void)
{
    /* Set port A pin 22 high to turn Arduino Mega red LED on */
    PORTA |= _BV(PORTA0);
    _delay_ms(BLINK_DELAY_MS);
    /* Set port A pin 22 high to turn Arduino Mega red LED off */
    PORTA &= ~_BV(PORTA0);
    _delay_ms(BLINK_DELAY_MS);
    /* Set port A pin 24 high to turn Arduino Mega green LED on */
    PORTA |= _BV(PORTA2);
    _delay_ms(BLINK_DELAY_MS);
    /* Set port A pin 24 high to turn Arduino Mega green LED off */
    PORTA &= ~_BV(PORTA2);
    _delay_ms(BLINK_DELAY_MS);
    /* Set port A pin 26 high to turn Arduino Mega blue LED on */
    PORTA |= _BV(PORTA4);
    _delay_ms(BLINK_DELAY_MS);
    /* Set port A pin 26 high to turn Arduino Mega blue LED off */
    PORTA &= ~_BV(PORTA4);
    _delay_ms(BLINK_DELAY_MS);
}

static inline void output_name(void)
{
    /* Print name */
    fprintf_P(stdout, PSTR(STUDENT_NAME));
    fputc('\n', stdout); /* Add a new line to the uart printout */
    lcd_puts_P(PSTR(STUDENT_NAME));
    /* End print name */
}

static inline void number_console(void)
{
    int x = 0;
    fprintf_P(stdout, PSTR(GET_NR_MSG));
    lcd_goto(0x40);

    if (scanf("%d", &x) == 1 && x >= 0 && x <= 9) {
        printf("\nYou entered number: ");
        fprintf_P(stdout, (PGM_P)pgm_read_word(&numbers[x]));
        fputc('\n', stdout);
        lcd_puts_P((PGM_P)pgm_read_word(&numbers[x]));
        lcd_putc(' ');
    } else {
        printf("invalid input\n");
    }
}


static inline void table_print(void)
{
    print_ascii_tbl(stdout);
    unsigned char ascii[128];

    for (unsigned char i = 0; i < sizeof(ascii); i++) {
        ascii[i] = i;
    }

    print_for_human(stdout, ascii, sizeof(ascii));
}


void main(void)
{
    /*INIT */
    init_leds();
    init_errcon();
    uart0_init();
    stdout = stdin = &uart0_io;
    lcd_init();
    lcd_clrscr();
    /* INIT END*/
    output_name();
    table_print();

    while (1) {
        blink_leds();
        number_console();
    }
}





























